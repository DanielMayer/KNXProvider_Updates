#!/bin/bash
SERVICE='eibd'
PROVIDER='java'
cmd='groupsocketlisten ip:localhost'

#Checks if eibd is ready by trying a command. Due to DHCP eibd will be reported as running some time before it is usable.
while true;
do
	if ! pgrep $SERVICE > /dev/null; 
	then
		echo "eibd not yet running."
		sleep 1
	else
		sleep 10
		while true; do
        		timeout 2 $cmd
        		RESULT=$?
        		echo $RESULT
        		# Use the below when you want the output not to contain some string
        		if [ $RESULT -eq 124 ]; 
        		then
                		if ! pgrep $PROVIDER > /dev/null; 
                		then
                        		echo "starting KNXProvider"
                        		cd /home/pi/KNXProvider && sudo -u pi nohup java -cp . at/dani/eibprovider/Activator >log/log.txt &
                        		break 2;
                		else
                        		echo "KNXProvider already running"
                        		break 2;
                		fi
                	echo "eibd Command successful"
                	break 2;
			else
				eval "sudo service eibd restart"
        		fi
        	sleep 1
		done
	fi
done

