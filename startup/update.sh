#!/bin/bash
DOWNLINK='https://gitlab.com/DanielMayer/KNXProvider_Updates/repository/archive.zip?ref=master'
OFILE='KNXProvider.zip'
OPath='/home/pi/KNXProvider/update/'

cd "${0%/*}"
cd ../update/
echo $(ls)
wget -O $OFILE $DOWNLINK
RESULT=$?
echo $RESULT
if [ $RESULT -eq 0 ];
then
	unzip $OFILE
	rm $OFILE
	CONTENT=$(ls)
	echo $CONTENT
	cd $CONTENT
	rsync -av -I * ../../
	cd ../
	echo $(ls)
	rm -rf $CONTENT
fi
